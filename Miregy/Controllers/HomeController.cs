﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Miregy.Models;
using Miregy.Models.FormModels;

namespace Miregy.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(new NewUser());
        }


        #region Kullanici kayit islemleri - User registration operations
        #region kayit formunu acar - opens the registration form
        public IActionResult RegisterNewUser()
        {
            return View(new NewUser());
        }
        #endregion

        #region gelen objeyi kayit eder - save the object
        public IActionResult Register(NewUser newUser)
        {
            return Ok();
        }
        #endregion
        #endregion
       
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }        
    }
}
