﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miregy.Models.FormModels
{
    public class NewUser
    {
        public string Ad { get; set; }
        public string SoyAd { get; set; }
        public string Yas { get; set; }
        public string Email { get; set; }
        public string Sirket { get; set; }
        public string Sube { get; set; }
        public string Rol { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
